var pfad = "";

var app = new Vue({

	el: "#root",
	// Erstinitialisierung der Parameter 
	data: {
		showingModal:false,
		showingeditModal: false,
		showingdeleteModal: false,
		errorMessage : "",
		successMessage : "",
		//Array aus PHP initialisieren
		Wohnung: [],
		Mieter: [],
		Vermieter: [],
		Zimmer: [],
		Bilder: [],
		files: [],
		maxSeiten: 1,
		aktSeite: 1,
		anzahlElementeProSeite:1,
		startElement: 0,
		maxElemente: 20,
		berechneteSeiten: 0,
		listItem: "",
		slideIndex:  [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
		slideId: ["","mySl1", "mySl2", "mySl3", "mySl4", "mySl5", "mySl6", "mySl7", "mySl8", "mySl9", "mySl10"],
		info: { name: '',image: '', },
		//Object Initialisierung für neue Wohnung
		neuWohnung: { 	
			WID: "",
			Objekt: "", 
			ObjektBeschreibung: "", 
			VermieterID: "", 
			Typ: "",
			Etage: "",
			Wohnflaeche: "",
			Nutzflaeche: "",
			Bezugsfreiab: "",
			ZimmerID: "",
			Schlafzimmer: "",
			Badezimmer: "",
			Haustiere: "",
			Kaltmiete: "",
			Nebenkosten: "",
			Heizkosten: "",
			Gesamtkosten: "",
			Baujahr: "",
			Modern_Sanierung: "",
			Objektzustand: "",
			Ausstattung: "",
			Heizungsart: "",
			Energietraeger: "",
			Energieausweis: "",
			EnergieausweisTyp: "",
			Energiekennwert: "",
			MieterID: "", 
			Einzugsdatum: "", 
			w_Strasse: "", 
			w_PLZ: "", 
			w_Ort: "", 
			w_Land: "", 
			w_email: "", 
			Token: "",
		},
		aktWohnung: {},
		mode: "create",
		
	},
	mounted: function () {
		// Aufruf der Function zum Einlesen der Wohnungen
		this.getAllWohnung();
	},
	methods: {
		// Function zum Einlesen aller Wohnung aus JSON
		getAllWohnung: function(){
			axios.get(pfad+"app.php?mode=read")
			.then(function(response){
				console.log("read");
				console.log(response);
				if (response.data.error) {
					app.errorMessage = response.data.message;
				}else{
					// Zuweisung der JSON elemente
					app.Wohnung = response.data.Wohnung;
					app.Zimmer = response.data.Zimmer;
					app.Bilder = response.data.Bilder;
					if (localStorage.getItem('anzahlElementeProSeite')){
						app.anzahlElementeProSeite = parseInt(JSON.parse(localStorage.getItem('anzahlElementeProSeite')));
					}else{
						app.anzahlElementeProSeite = response.data.anzahlElementeProSeite
					}
					app.maxSeiten = response.data.maxSeiten;
					app.maxElemente=20;
					app.aktSeite = 1;
					app.property = response.data.property;
					app.val = response.data.val;
					app.startElement = 0;
					app.berechneteSeiten = Math.ceil(response.data.maxElemente / app.anzahlElementeProSeite);
					app.slideIndex = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
					app.slideId = ["","mySl1", "mySl2", "mySl3", "mySl4", "mySl5", "mySl6", "mySl7", "mySl8", "mySl9", "mySl10"];
				}
			});
		},
		// Auswahl der Wohnung, neuWohnung oder aktWohnung
		selectWohnung(Wohnung){ app.aktWohnung = Wohnung;	},
		// Formulardaten einlesen
		toFormData: function(obj){
			var form_data = new FormData();
		    for ( var key in obj ) {
		        form_data.append(key, obj[key]);
		    } 
		    return form_data;
		},
		// Meldungen löschen
		clearMessage: function(){
			app.errorMessage = "";
			app.successMessage = "";
		},
		    selectPrev:function(){
   		if (app.aktSeite > 1){
			app.aktSeite--;
		}else{
			app.aktSeite=app.maxSeiten;
		}
		return app.aktSeite;
    },
    selectPrev:function(){
   		if (app.aktSeite > 1){
			app.aktSeite--;
		}else{
			app.aktSeite=app.berechneteSeiten;
		}
		if(app.berechneteSeiten > app.maxSeiten){
			app.startElement = (Math.ceil( app.aktSeite / app.maxSeiten)-1) * app.maxSeiten;
		}
		return app.aktSeite;
    },
    selectAkt:function(akt){
			app.aktSeite = akt;
			if( akt ==1 || akt == app.berechneteSeiten){
				app.startElement = (Math.ceil( app.aktSeite / app.maxSeiten)-1) * app.maxSeiten;
			}
			return app.aktSeite;
    },
    selectNext:function(){
   		if (app.aktSeite < app.berechneteSeiten){
			app.aktSeite++;
		}else{
			app.aktSeite=1;
		}
		if(app.berechneteSeiten > app.maxSeiten){
			app.startElement = (Math.ceil( app.aktSeite / app.maxSeiten)-1) * app.maxSeiten;
		}
		return app.aktSeite;
    },
	saveAnzahlElemente:function(val){
		app.anzahlElementeProSeite = val;
		localStorage.setItem('anzahlElementeProSeite', JSON.stringify(app.anzahlElementeProSeite));
		return app.getAllWohnung();
	},
    addFiles(){
        this.$refs.files.click();
    },
	
	plusSlide:function(n, no) {
	  app.showSlide(app.slideIndex[no] += n, no);
	},

	showSlide:function(n, no) {
	  var i;
	  var x = document.getElementsByClassName(app.slideId[no]);
	  if (n > x.length) {app.slideIndex[no] = 1}    
	  if (n < 1) {app.slideIndex[no] = x.length}
	  for (i = 0; i < x.length; i++) {
		 x[i].style.display = "none";  
	  }
	  console.log(app.slideIndex[no]-1)
	  x[app.slideIndex[no]-1].style.display = "block";  
/*      setTimeout(function () {
		app.plusSlide(app.slideIndex[no],no);
         }, 2000);	*/
	},
  }
});