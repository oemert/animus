<?php 


include ("verbindung/config.php");

global $link;
$res = array('error' => false);
if (isset($_GET['val']))$val = $_GET['val'];

$mode = 'read'; if (isset($_GET['mode'])) $mode = $_GET['mode'];

$res['message'] = "";
$res['post_mode'] = $mode; //Array zur über mittels JSON

// Einlesen der Daten aus der Datenbank
	// Alle Relevanten elemente aus der Tabelle Wohnung auslesen
	$wohnung_sql = "
	SELECT @row_number:=@row_number+1 AS Zeile, w.WID, w.Objekt, w.ObjektBeschreibung, w.w_Strasse, w.w_PLZ, w.w_Ort, w.w_Land, w.w_email, 
		w.Typ, w.Qm, w.Etage, w.Wohnflaeche, w.Nutzflaeche, w.Bezugsfreiab, w.ZimmerID, w.Schlafzimmer, w.Badezimmer, w.Haustiere, 
		w.Kaltmiete, w.Nebenkosten, w.Heizkosten, w.Gesamtkosten, 
		w.Baujahr, w.Modern_Sanierung, w.Objektzustand, w.Ausstattung, w.Heizungsart, 
		w.Energietraeger, w.Energieausweis, w.EnergieausweisTyp, w.Energiekennwert,
		z.Anzahl
  		FROM wohnung as w, (SELECT @row_number:=0) AS t 
		JOIN zimmer AS z WHERE w.ZimmerID = z.ZID
		";
	$zimmer_sql	= "SELECT ZID, Anzahl FROM zimmer";
	$bilder_sql	= "
	SELECT  
		@row_number:=CASE
    WHEN @customer_no = b.WID THEN @row_number + 1
    ELSE 1
	END AS Zeile,
		@customer_no:=b.WID as bWID,
		b.WID,
		b.BID, 
		b.Name, 
		b.Frontpicture,  
		Concat('images/',b.Uploadname,'.',b.Typ) as File FROM bilder as b
		,(SELECT @customer_no:=0,@row_number:=0) as t
	ORDER BY b.WID ASC";
	$arrWohnung = readData($link, $wohnung_sql, 	"Wohnung");
	$res['Wohnung'] = $arrWohnung;
	$res['maxSeiten'] = 10;
	$res['val'] = 1;
	$res['startElement'] = 0;
	$res['maxElemente'] = count($arrWohnung);
	$res['anzahlElementeProSeite'] = 10;
	$res['berechneteSeiten'] = ceil(count($arrWohnung)/$res['anzahlElementeProSeite']);

	// Aufruf der SQL_Strings mittels der Function readData
	readData($link, $zimmer_sql, 	"Zimmer");
	readData($link, $bilder_sql, 	"Bilder");

	//Function zum Einlesen der Daten für die Übergabe als JSON
	function readData($link, $sql, $resdata){
		global $res;
		//Sichergehen das die Umlaute auch richtig eingelesen werden.
		mysqli_query($link,"SET NAMES 'utf8'");
		$arr = array();
		if ($result = mysqli_query($link, $sql)) {
			while ($row = mysqli_fetch_assoc($result)) {
				array_push($arr, $row); // Tabelleninhalt in array einfügen
			}
			$res[$resdata] = $arr; //Array zur über mittels JSON
			mysqli_free_result($result);
		}else{
			$res['error'] = true;
			$res['message'] .= $sql.":".$result;
		}
		return $arr;
	}
	header("Content-type: application/json");
	echo json_encode($res); // Übergabe Array an JSON
	die();

 ?>