<?php 
// Verbindung zur Datenbank
session_start();
$token = "";
if (isset($_SESSION["token"])){
	$token = " WHERE Token = '".$_SESSION["token"]."'";
}
header("Content-Type: multipart/form-data");
include ("functions.php");
include ("verbindung/config.php");
require 'phpmailer/class.smtp.php';
require 'phpmailer/class.phpmailer.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

global $link;

$res = array('error' => false);

$mode = 'read'; if (isset($_GET['mode'])) $mode = $_GET['mode'];
if (isset($_GET['val']))$val = $_GET['val'];

$res['message'] = "";
$res['post_mode'] = $mode; //Array zur über mittels JSON

// Einlesen der Daten aus der Datenbank
if ($mode == 'read') {
	// Alle Relevanten elemente aus der Tabelle Wohnung auslesen
	$wohnung_sql = "
		SELECT @row_number:=@row_number+1 AS Zeile, WID, Objekt, ObjektBeschreibung, w_Strasse, w_PLZ, w_Ort, w_Land, w_email, 
		Typ, Etage, Wohnflaeche, Nutzflaeche, Bezugsfreiab, ZimmerID, Schlafzimmer, Badezimmer, Haustiere, 
		Kaltmiete, Nebenkosten, Heizkosten, Gesamtkosten, Baujahr, Modern_Sanierung ,Objektzustand, Ausstattung,
		Heizungsart, Energietraeger, Energieausweis, EnergieausweisTyp, Energiekennwert, 
		'' as Picture
		FROM wohnung, (SELECT @row_number:=0) AS t $token";
	$zimmer_sql 	= "SELECT ZID, Anzahl FROM zimmer";
	// Alle Relevanten elemente aus der Tabelle Bilder auslesen
	$bilder_sql 	= "SELECT BID, Name, Uploadname, Typ, Frontpicture, WID, Concat('../images/',Uploadname,'.',Typ) AS File FROM bilder ORDER BY WID ASC, Frontpicture DESC";
	
	// Aufruf der SQL_Strings mittels der Function readData
	$arrWohnung = readData($link, $wohnung_sql, 	"Wohnung");
	$res['Wohnung'] = $arrWohnung;
	$res['maxPages'] = 10;
	$res['startElement'] = 0;
	$res['maxElements'] = count($arrWohnung);
	$res['maxElementsPerPage'] = 10;
	$res['calculatedPages'] = ceil(count($arrWohnung)/$res['maxElementsPerPage']);
	readData($link, $zimmer_sql, 	"Zimmer");
	readData($link, $bilder_sql, 	"Picture");
}


	//Function zum Einlesen der Daten für die Übergabe als JSON
	function readData($link, $sql, $resdata){
		global $res;
		//Sichergehen das die Umlaute auch richtig eingelesen werden.
		mysqli_query($link,"SET NAMES 'utf8';");
		$arr = array();
		if ($result = mysqli_query($link, $sql)) {
			while ($row = mysqli_fetch_assoc($result)) {
				array_push($arr, $row); // Tabelleninhalt in array einfügen
			}
			$res[$resdata] = $arr; //Array zur über mittels JSON
			mysqli_free_result($result);
		}else{
			$res['error'] = true;
			$res['message'] .= $sql;
		}
		return $arr;
	}
	function getPostData($link, $post ){
		return mysqli_real_escape_string($link, $_POST[$post]);
	}

	if ($mode == 'create' || $mode == 'update' ) {
		//Parameter des POSTS einlesen
		$mail_text = "";
		mysqli_query($link,"SET NAMES 'utf8'");
		$result = mysqli_query($link,"SHOW COLUMNS FROM wohnung");
		$sql = "INSERT INTO `wohnung` ( ";
		$val = " ) VALUES ( ";
		if ($mode == 'create') {
			while ($row = mysqli_fetch_assoc($result)) {
				$feld = $row['Field'];
				if( strripos($feld,'ID') === false && isset($_POST[$feld]) ){
					$sql .= " `".$feld."`, ";
					$val .= " '".getPostData($link, $feld)."', ";
					$mail_text .= $feld." = ".getPostData($link, $feld)."<br>";
				}
			}
		}elseif($mode == 'update'){
			$sql = "UPDATE wohnung SET  ";
			while ($row = mysqli_fetch_assoc($result)) {
				$feld = $row['Field'];
				if( strripos($feld,'ID') === false && isset($_POST[$feld]) ){
					$sql .= " `".$feld."` = '".getPostData($link, $feld)."', ";
				}
			}
			$val = "   ";
		}
		$sql = substr($sql,0,-2);
		$val = substr($val,0,-6);
		$token_neu = zufallsstring(30);
		if ($mode == 'create')$sql .= $val."'".$token_neu."' );";
		if ($mode == 'update')$sql .= " where WID=".getPostData($link, 'WID').";";

		// Neue Wohnung hinzufügen
		if ($mode == 'create') {
			//Neuen Datensatz in die Tabelle einfügen
			$result = mysqli_query($link,$sql);
			if ($result) {
				$mail = new PHPMailer;
				$mail->isSMTP();
				$mail->SMTPDebug = 0;
				$mail->Host = 'mxf990.netcup.net';
				$mail->Port = 587;
				$mail->SMTPAuth = true;
				$mail->Username = 'animus@turkgeldi.de';
				$mail->Password = 'Wp08kl&8';
				$mail->setFrom('omer@turkgeldi.de', uml_replace('Ömer Türkgeldi'));
				$mail->addReplyTo('omer@turkgeldi.de', uml_replace('Ömer Türkgeldi'));
				$mail->addBCC(uml_replace(trim(getPostData($link, 'w_email')), ""));
				$mail->Subject = 'Neuer eintrag  '.date('Y-m');
				$mail->msgHTML( uml_replace( "Link zum bearbeiten oder löschen.<br>".str_replace("#ersetze",$token_neu,$_SESSION['link_url'])."<br>".$mail_text	));
				if (!$mail->send()) {
					$res['message'] =  $mail->ErrorInfo."\r\n";
				} else {
					$res['message'] =  "Email erfolgreich gesendet.\r\n";
				}
				$res['message'] .= "Wohnung erfolgreich angelegt...";
			} else{
				$res['error'] = true;
				$res['message'] .= "Wohnung einfügen ist gescheitert!!! \r\n".$sql;
			}
		}

		// Änderungen am Objekt in die Datenbank schreiben
		if ($mode == 'update') {
			//Änderungen in die Tabelle schreiben
			$result = mysqli_query($link,$sql);
			if ($result) {
				$res['message'] = "Änderungen erfolgreich übernommen";
			} else{
				$res['error'] = true;
				$res['message'] = "Änderungsversuch schlug fehl!!!";
				$res['message'] .= $sql;
			}
		}
	}

	// Objekt löschen
	if ($mode == 'delete') {
		$id = getPostData($link, 'WID');
		// Datensatz aus der Tabelle löschen
		$result = mysqli_query($link,"DELETE FROM `wohnung` WHERE `WID` = '$id'");
		if ($result) {
			$res['message'] = "Löschen erfolgreich...";
		} else{
			$res['error'] = true;
			$res['message'] = "Löschen fehlgeschlagen";
		}
	}

	if($mode == "deletePicture"){
		$id = mysqli_real_escape_string($link, $_POST['WID']);
		$name = mysqli_real_escape_string($link, $_POST['Picture']);
		$query = "DELETE FROM bilder WHERE WID = $id && Uploadname = '".substr($name,0,-4)."' ";
		$res['deletePic'] = "$query";
		if (mysqli_query($link,$query)){
			unlink('../images/'.$name);
			$res['deletePic'] = "Bild $name erfolgreich gelöscht. $query";
		}
	}	
	if($mode == "saveFrontPicture"){
		$id = mysqli_real_escape_string($link, $_POST['WID']);
		$name = mysqli_real_escape_string($link, $_POST['Picture']);
		$query = "UPDATE bilder  SET Frontpicture = 0 WHERE WID = $id ";
		if (mysqli_query($link,$query)){
			$query = "UPDATE bilder  SET Frontpicture = 1 WHERE WID = $id && Uploadname = '".substr($name,0,-4)."' ";
			if (mysqli_query($link,$query)){
			}
		}
	}	

	header("Content-type: application/json");
	echo json_encode($res); // Übergabe Array an JSON
	die();

 ?>