var pfad = "";

var app = new Vue({

	el: "#root",
	// Erstinitialisierung der Parameter 
	data: {
		showingModal:false,
		showingeditModal: false,
		showingdeleteModal: false,
		errorMessage : "",
		successMessage : "",
		//Array aus PHP initialisieren
		Wohnung: [],
		Mieter: [],
		Vermieter: [],
		Zimmer: [],
		Picture: [],
		files: [],
		maxPages: 1,
		actPage: 1,
		maxElementsPerPage:1,
		startElement: 0,
		maxElements: 20,
		calculatedPages: 0,
		deletePic:"",
		url: null,
		//Object Initialisierung für neue Wohnung
		newWohnung: { 	
			WID: "",
			Objekt: "", 
			ObjektBeschreibung: "", 
			VermieterID: "", 
			Typ: "",
			Etage: "",
			Wohnflaeche: "",
			Nutzflaeche: "",
			Bezugsfreiab: "",
			ZimmerID: "",
			Schlafzimmer: "",
			Badezimmer: "",
			Haustiere: "",
			Kaltmiete: "",
			Nebenkosten: "",
			Heizkosten: "",
			Gesamtkosten: "",
			Baujahr: "",
			Modern_Sanierung: "",
			Objektzustand: "",
			Ausstattung: "",
			Heizungsart: "",
			Energietraeger: "",
			Energieausweis: "",
			EnergieausweisTyp: "",
			Energiekennwert: "",
			MieterID: "", 
			Einzugsdatum: "", 
			w_Strasse: "", 
			w_PLZ: "", 
			w_Ort: "", 
			w_Land: "", 
			w_email: "", 
			Token: "",
		},
		actWohnung: {},
		mode: "create",
		
	},
	mounted: function () {
		// Aufruf der Function zum Einlesen der Wohnungen
		this.getAllWohnung();
	},
	methods: {
		// Function zum Einlesen aller Wohnung aus JSON
		getAllWohnung: function(actPage){
			axios.get(pfad+"app_admin.php?mode=read")
			.then(function(response){
				console.log("read");
				console.log(response);
				if (response.data.error) {
					app.errorMessage = response.data.message;
				}else{
					// Zuweisung der JSON elemente
					app.Wohnung = response.data.Wohnung;
					app.Zimmer = response.data.Zimmer;
					app.Picture = response.data.Picture;
					if (localStorage.getItem('maxElementsPerPage')){
						app.maxElementsPerPage = parseInt(JSON.parse(localStorage.getItem('maxElementsPerPage')));
					}else{
						app.maxElementsPerPage = response.data.maxElementsPerPage
					}
					app.maxPages = response.data.maxPages;
					app.maxElements=20;
					app.property = response.data.property;
					app.val = response.data.val;
					app.startElement = 0;
					app.calculatedPages = Math.ceil(response.data.maxElements / app.maxElementsPerPage);
					app.deletePic = response.data.deletePic;
					app.files = [];
					app.selectActPage(actPage == null ? 1 : actPage);
					app.url = "";
				}
			});
		},
		// Funtion zum hinzufügen einer neuen Wohnung
		saveWohnung:function(){
			// Einlesen der neuen Daten zur Übergabe mittles Post
			var formData = app.toFormData(app.newWohnung);
			axios.post(pfad+"app_admin.php?mode=create", formData)
				.then(function(response){
					console.log(response);
					app.newWohnung = { 	
						WID: "",
						Objekt: "", 
						ObjektBeschreibung: "", 
						VermieterID: "", 
						Typ: "",
						Etage: "",
						Wohnflaeche: "",
						Nutzflaeche: "",
						Bezugsfreiab: "",
						ZimmerID: "",
						Schlafzimmer: "",
						Badezimmer: "",
						Haustiere: "",
						Kaltmiete: "",
						Nebenkosten: "",
						Heizkosten: "",
						Gesamtkosten: "",
						Baujahr: "",
						Modern_Sanierung: "",
						Objektzustand: "",
						Ausstattung: "",
						Heizungsart: "",
						Energietraeger: "",
						Energieausweis: "",
						EnergieausweisTyp: "",
						Energiekennwert: "",
						MieterID: "", 
						Einzugsdatum: "", 
						w_Strasse: "", 
						w_PLZ: "", 
						w_Ort: "", 
						w_Land: "", 
						w_email: "", 
						Token: "",
					};
					app.mode = "create";
					console.log("insert");
					if (response.data.error) {
						app.errorMessage = response.data.message;
					}else{
						app.successMessage = response.data.message;
						app.getAllWohnung();
					}
				});
		},
			// Funtion um Änderungen an der Wohnung zu speichern
		updateWohnung:function(){
			// Einlesen der geänderten Daten zur Übergabe mittles Post
			var formData = app.toFormData(app.actWohnung);
			let aktPage = app.actPage;
			axios.post(pfad+"app_admin.php?mode=update", formData)
				.then(function(response){
					console.log("update");
					app.mode = "update";
					if (response.data.error) {
						app.errorMessage = response.data.message;
					}else{
						app.successMessage = response.data.message;
						app.getAllWohnung(aktPage);
						console.log(aktPage);
					}
				});
		},
		// Funtion um eine Wohnung zu löschen
	deleteWohnung:function(){
		// Einlesen der geänderten Daten zur Übergabe mittles Post
		var formData = app.toFormData(app.actWohnung);
		axios.post(pfad+"app_admin.php?mode=delete", formData)
			.then(function(response){
				console.log("delete");
				app.mode = "delete";
				app.actWohnung = {};
				if (response.data.error) {
					app.errorMessage = response.data.message;
				}else{
					app.successMessage = response.data.message;
					app.getAllWohnung();
				}
			});
		},
		
		// Auswahl der Wohnung, newWohnung oder actWohnung
	selectWohnung(Wohnung){ app.actWohnung = Wohnung;	},
		// Formulardaten einlesen
	toFormData: function(obj){
		var form_data = new FormData();
	    for ( var key in obj ) {
	        form_data.append(key, obj[key]);
	    } 
	    return form_data;
	},
		// Meldungen löschen
	clearMessage: function(){
		app.errorMessage = "";
		app.successMessage = "";
	},
		
	addFiles(){
        this.$refs.files.click();
    },

      /*
        Submits files to the server
      */
    submitFiles(){
		var formData = new FormData();
		var files = this.files;
		let actPage = app.actPage;
		for( var i = 0; i < files.length; i++ ){
			let file = files[i];
			formData.append('files[' + i + ']', file);
        }
		console.log("upload");
        axios.post( pfad+"upload.php?WID="+app.actWohnung['WID'],
			formData,{
				headers: {	'Content-Type': 'multipart/form-data'	}
			}
        ).then(function(response){
		console.log(response);
		app.getAllWohnung(actPage);
  		app.selectWohnung(app.actWohnung);
        })
        .catch(function(response){
			console.log('FAILURE!!'+response);
        });
    },
    saveFront(Picture){
		app.actWohnung['Picture'] = Picture.split('/')[2];
		var formData = app.toFormData(app.actWohnung);
		var actSave = app.actWohnung;
		let actPage = app.actPage;
		app.mode = "saveFrontPicture";
		axios.post(pfad+"app_admin.php?mode=saveFrontPicture", formData)
			.then(function(response){
				console.log("saveFrontPicture");
				app.mode = "saveFrontPicture";
				if (response.data.error) {
					app.errorMessage = response.data.message;
				}else{
					app.successMessage = response.data.message;
					app.getAllWohnung(actPage);
					app.selectWohnung(actSave);
				}
			});		
    },
	
    deletePicture(Picture){
		app.actWohnung['Picture'] = Picture.split('/')[2];
		var formData = app.toFormData(app.actWohnung);
		var actSave = app.actWohnung;
		let actPage = app.actPage;
		app.mode = "deletePicture";
		axios.post(pfad+"app_admin.php?mode=deletePicture", formData)
			.then(function(response){
				console.log("deletePicture");
				app.mode = "deletePicture";
				if (response.data.error) {
					app.errorMessage = response.data.message;
				}else{
					app.successMessage = response.data.message;
					app.getAllWohnung(actPage);
					app.selectWohnung(actSave);
				}
			});		
    },

	/*
        Handles the uploading of files
      */
      handleFilesUpload(e){
        let uploadedFiles = this.$refs.files.files;

        /*
          Adds the uploaded file to the files array
        */
        for( var i = 0; i < uploadedFiles.length; i++ ){
			this.files.push( uploadedFiles[i] );
		}
		app.previewUpload();
      },
     /*
        Removes a select file the user has uploaded
      */
	previewUpload:function(){
		for (let i=0; i<this.files.length; i++){
			let reader = new FileReader(); //instantiate a new file reader
			reader.addEventListener('load', function(){
			  this.$refs['image' + parseInt( i )][0].src = reader.result;
			}.bind(this), false);  //add event listener
			reader.readAsDataURL(this.files[i]);
		}		

	},
	removeFile:function( key ){
        this.files.splice( key, 1 );
		app.previewUpload();
    },

	selectPrevPage:function(){
   		if (app.actPage > 1){
			app.actPage--;
		}else{
			app.actPage=app.calculatedPages;
		}
		app.selectActPage(app.actPage)
		return app.actPage;
    },
    
	selectActPage:function(act){
			app.actPage = act;
			app.startElement = (Math.ceil( app.actPage / app.maxPages)-1) * app.maxPages;
			return app.actPage;
    },
    
	selectNextPage:function(){
   		if (app.actPage < app.calculatedPages){
			app.actPage++;
		}else{
			app.actPage=1;
		}
		app.selectActPage(app.actPage)
		return app.actPage;
    },
	
	saveMaxlElements:function(val){
		app.maxElementsPerPage = val;
		localStorage.setItem('maxElementsPerPage', JSON.stringify(app.maxElementsPerPage));
		return app.getAllWohnung();
	},
	
 	}
});