<?php
   session_start();
   unset($_SESSION["username"]);
   unset($_SESSION["password"]);
   
   echo 'Sie wurden abgemeldet.';
   header('Refresh: 2; URL = index.php');
?>