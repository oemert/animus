<?php
	include ("verbindung/config.php");
	global $link;
	ob_start();
	session_start();
?>

<?
   // error_reporting(E_ALL);
   // ini_set("display_errors", 1);
?>

<html lang = "en">
   
   <head>
      <title>Animus</title>
      <link href = "css/bootstrap.min.css" rel = "stylesheet">
      
      <style>
         body {
            padding-top: 40px;
            padding-bottom: 40px;
            background-color: #ADABAB;
         }
         
         .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
            color: #017572;
         }
         
         .form-signin .form-signin-heading,
         .form-signin .checkbox {
            margin-bottom: 10px;
         }
         
         .form-signin .checkbox {
            font-weight: normal;
         }
         
         .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
         }
         
         .form-signin .form-control:focus {
            z-index: 2;
         }
         
         .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
            border-color:#017572;
         }
         
         .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
            border-color:#017572;
         }
         
         h2{
            text-align: center;
            color: #017572;
         }
      </style>
      
   </head>
	
   <body>
      
      <div class = "container form-signin">
         
         <?php
            $msg = '';
			error_reporting(0);
			$token = mysqli_real_escape_string(explode("token=",$_SERVER["QUERY_STRING"])[1]);
			error_reporting(1);
			if( $token != "" ){
				mysqli_query($link,"SET NAMES 'utf8'");
				$sql_token = "SELECT w_email, Token FROM wohnung WHERE Token = '$token'"	;
				if ($result = mysqli_query($link, $sql_token)) {
					$row = mysqli_fetch_assoc($result);
					$email = $row['w_email'];
					if ( $email == "" )header("Location: index.php");
					$sql_user = "SELECT username from users WHERE email = '$email' "	;
					if ($result = mysqli_query($link, $sql_user)) {
						$row = mysqli_fetch_assoc($result);
						$username = $row['username'];
						mysqli_free_result($result);
					}
					$_SESSION['valid'] = true;
					$_SESSION['timeout'] = time();
					$_SESSION['username'] = $username;
					$_SESSION['token'] = $token;
					header("Location: wohnung.php");
				}
			}else{
				if (isset($_POST['login']) && !empty($_POST['username']) 
				   && !empty($_POST['password'])) {
					$post_username = $_POST['username'];
					$sql = "SELECT username, password FROM users WHERE username = '$post_username'"	;
					if ($result = mysqli_query($link, $sql)) {
						$row = mysqli_fetch_assoc($result);
						$users_username = rtrim($row["username"]);
						mysqli_free_result($result);
					}
 				    if ($post_username == $users_username && 
					  md5($_POST['password']) == $row["password"]) {
					  $_SESSION['valid'] = true;
					  $_SESSION['timeout'] = time();
					  $_SESSION['username'] = $users_username;
					  $_SESSION['link_url'] = $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']."?token=#ersetze";
					  header("Location: wohnung.php");
				    }else {
					  $msg = 'Benuzername oder Passwort falsch!';
				    }
				}
			}
         ?>
      </div> <!-- /container -->
      
      <div class = "container">
      
         <form class = "form-signin" role = "form" 
            action = "<?php echo htmlspecialchars($_SERVER['PHP_SELF']); 
            ?>" method = "post">
            <h4 class = "form-signin-heading"><?php echo $msg; ?></h4>
           <h3>Benutzername: </h3> <input type = "text" class = "form-control" 
               name = "username" placeholder = "Benutzername" 
               required autofocus></br>
            <h3>Passwort: </h3><input type = "password" class = "form-control"
               name = "password" placeholder = "Passwort" required>
            <button class = "btn btn-lg btn-primary btn-block" type = "submit" 
               name = "login">Anmelden</button>
         </form>
	    
      </div> 
      
   </body>
</html>