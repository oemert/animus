<?php
session_start();//session starten
if(!isset($_SESSION["username"])){ 
    header("Location: index.php"); 
    exit;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv=“cache-control“ content=“no-cache“>
	<meta http-equiv=“pragma“ content=“no-cache“>
	<meta http-equiv=“expires“ content=“0″>
	<title>Wohnungsverwaltung </title>
	<link type='text/css'  rel="stylesheet" href="css/bootstrap.min.css">
	<link type='text/css'  rel="stylesheet" href="css/material.css">
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/axios.min.js"></script>
	<link type='text/css'  rel="stylesheet" href="css/pagination.css">
	<link type='text/css'  rel="stylesheet" href="css/style.css">
</head>
<script>

</script>
<body>
	<div id="root">
		<div class="">
			<div class="crud_header">
				<h1 class="left"><image src="images/animus.png"/> &nbsp; Wohnungsverwaltung</h1>
				<div id="logout"><a  href = "logout.php" title="Abmelden"><img src="images/abmelden.png" /></a></div>
				<button class="right addnew" @click="showingModal = true;">Neue Wohnung</button>
				<div class="fix"></div>
			</div>
			<p class="errorMessage" v-if="errorMessage">{{errorMessage}}</p>
			<p class="successMessage" v-if="successMessage">{{successMessage}}</p>
			<div class="paginationjs paginationjs-big paginationjs-theme-blue">
				<div style="float:right;">Anzahl Elemente in Liste: &nbsp;<input  style="text-align:right;width:50px" type="number" v-bind:value="maxElementsPerPage" @change="saveMaxlElements($event.target.value)"/></div>
				<div class="paginationjs-pages">
					<ul v-if="calculatedPages > 1">
						<li class='paginationjs-prev' @click='selectPrevPage()' ><a >«</a></li>
						<li :class="`paginationjs-page J-paginationjs-page ${actPage == 1 ? 'active' : ''}`" v-for="z in Wohnung" v-if="z.Zeile == 1"  @click="selectActPage(1)"><a>1</a></li>
						<li class="paginationjs-page J-paginationjs-page disabled"  v-for="z in Wohnung" v-if="z.Zeile == maxPages+2 && startElement > 0 " ><a >:::</a></li>
						<li :class="`paginationjs-page J-paginationjs-page ${ parseInt(z.Zeile)+parseInt(startElement) == actPage  && actPage != 1 ? 'active' : ''}`" v-for="z in Wohnung" v-if="parseInt(z.Zeile)+parseInt(startElement) > 1 && z.Zeile <= maxPages && parseInt(z.Zeile)+parseInt(startElement) < calculatedPages "  @click="selectActPage(parseInt(z.Zeile)+parseInt(startElement) )"><a>{{ parseInt(z.Zeile)+parseInt(startElement) }}</a></li>
						<li class="paginationjs-page J-paginationjs-page disabled"  v-for="z in Wohnung" v-if="z.Zeile == maxPages-1  && calculatedPages > maxPages && actPage < parseInt(calculatedPages) - (parseInt(calculatedPages) % parseInt(maxPages))+1 " ><a >:::</a></li>
						<li :class="`paginationjs-page paginationjs-last J-paginationjs-page ${z.Zeile == actPage ? 'active' : ''}`"  v-for="z in Wohnung" v-if="z.Zeile == calculatedPages" @click="selectActPage(calculatedPages)"><a >{{calculatedPages}}</a></li>
						<li class='paginationjs-next J-paginationjs-next' title='Next page'  @click='selectNextPage()'><a >»</a></li>
					</ul>
				</div>
			</div>
			<table class="list">
				<tr>
					<th>ID</th>
					<th>WohnungID</th>
					<th>Beschreibung</th>
					<th>Strasse</th>
					<th>PLZ</th>
					<th>Ort</th>
					<th colspan="2"></th>
				</tr>
				<tr v-for="W in Wohnung"  v-if="W.Zeile > maxElementsPerPage*(actPage-1) && W.Zeile <= maxElementsPerPage*actPage " @dblclick="showingeditModal = true; selectWohnung(W)">
					<td>{{W.WID}}</td>
					<td>
					<a style="float:left;"><img v-for="key in Picture " class="Picture" v-bind:src="key.File" style="height:50px;width:50px;display:inline"  v-if="key.WID == W.WID && key.Frontpicture == 1" /></a>
					{{W.Objekt}}
					</td>
					<td style="width:500px;max-height:50px;height:50px;overflow:hidden">{{W.ObjektBeschreibung.substr(0,120)}}</td>
					<td>{{W.w_Strasse}}</td>
					<td>{{W.w_PLZ}}</td>
					<td>{{W.w_Ort}}</td>
					<td><button @click="showingeditModal = true; selectWohnung(W)">Bearbeiten</button></td>
					<td><button @click="showingdeleteModal = true; selectWohnung(W)" >Löschen</button></td>
				</tr>
			</table>
			<div class="fix"></div>
			<div class="modal col-md-10" id="addmodal" v-if="showingModal">
				<div class="modalheading">
					<p class="left">Neue Wohnung einfügen</p>
					<p class="right close" @click="showingModal = false;">x</p>
					<div class="fix"></div>
				</div>
				<div class="modalbody">
					<div class="modalcontent">
						<vue-tabs active-tab-color="#0074E8"  active-text-color="white">
							<v-tab title="Objekt-Daten">
								<table class="form">
									<tr>
										<th>WohnungID</th>
										<th>:</th>
										<td><input type="text" placeholder="Wohnung" v-model="newWohnung.Objekt"/></td>
									</tr>
									<tr>
										<th>Beschreibung</th>
										<th>:</th>
										<td><textarea type="text" placeholder="Beschreibung" v-model="newWohnung.ObjektBeschreibung"></textarea></td>
									</tr>
									<tr>
										<th>Strasse</th>
										<th>:</th>
										<td><input type="text" placeholder="Straße" v-model="newWohnung.w_Strasse"/></td>
									</tr>
									<tr>
										<th>PLZ</th>
										<th>:</th>
										<td><input type="text" placeholder="PLZ" v-model="newWohnung.w_PLZ"/></td>
									</tr>
									<tr>
										<th>Ort</th>
										<th>:</th>
										<td><input type="text" placeholder="Ort" v-model="newWohnung.w_Ort"/></td>
									</tr>
									<tr>
										<th>Land</th>
										<th>:</th>
										<td><input type="text" placeholder="Land" v-model="newWohnung.w_Land"/></td>
									</tr>
									<tr>
										<th>Kontakt E-Mail Adresse</th>
										<th>:</th>
										<td><input type="email" placeholder="Email" v-model="newWohnung.w_email"/></td>
									</tr>
								</table>
							</v-tab>
							<v-tab title="Objekt-Details">
								<table class="form">
									<tr>
										<th>Bezugsfrei ab</th>
										<th>:</th>
										<td><input type="date"  v-model="newWohnung.Bezugsfreiab"/></td>
									</tr>
									<tr>
										<th>Objektzustand</th>
										<th>:</th>
										<td><input type="text" placeholder="Objektzustand" v-model="newWohnung.Objektzustand"/></td>
									</tr>
									<tr>
										<th>Baujahr</th>
										<th>:</th>
										<td><input type="number" placeholder="Baujahr" v-model="newWohnung.Baujahr"/></td>
									</tr>
									<tr>
										<th>Modernisiert am<br>Saniert am</th>
										<th>:</th>
										<td><input type="date" v-model="newWohnung.Modern_Sanierung"/></td>
									</tr>
									<tr>
										<th>Typ</th>
										<th>:</th>
										<td><input type="text"  v-model="newWohnung.Typ"/></td>
									</tr>
									<tr>
										<th>Etage</th>
										<th>:</th>
										<td><input type="text" placeholder="Etage" v-model="newWohnung.Etage"/></td>
									</tr>
									<tr>
										<th>Wohnfläche</th>
										<th>:</th>
										<td><input type="number" placeholder="Wohnfläche" v-model="newWohnung.Wohnflaeche"/></td>
									</tr>
									<tr>
										<th>Nutzfläche</th>
										<th>:</th>
										<td><input type="number" placeholder="Nutzfläche" v-model="newWohnung.Nutzflaeche"/></td>
									</tr>
									<tr>
										<th>Zimmer</th>
										<th>:</th>
										<td>
											<select v-model="newWohnung.ZimmerID">
												<option v-for="Z in Zimmer" v-bind:value="Z.ZID" selected="newWohnung.ZimmerID == Z.ZID ? 'selected' : ''">{{Z.Anzahl}}</option>
											</select>
										</td>
									</tr>
									<tr>
										<th>Schlafzimmer</th>
										<th>:</th>
										<td><input type="number" placeholder="Schlafzimmer" v-model="newWohnung.Schlafzimmer"/></td>
									</tr>
									<tr>
										<th>Badezimmer</th>
										<th>:</th>
										<td><input type="number" placeholder="Badezimmer" v-model="newWohnung.Badezimmer"/></td>
									</tr>
									<tr>
										<th>Haustiere</th>
										<th>:</th>
										<td><input type="text" placeholder="Haustiere" v-model="newWohnung.Haustiere"/></td>
									</tr>
								</table>
							</v-tab>
							<v-tab title="Energiedetails">
								<table class="form">
									<tr>
										<th>Heizungssart</th>
										<th>:</th>
										<td><input type="text" placeholder="Heizungssart" v-model="newWohnung.Heizungssart"/></td>
									</tr>
									<tr>
										<th>Energieträger</th>
										<th>:</th>
										<td><input type="text" placeholder="z.B. Gas" v-model="newWohnung.Energietraeger"/></td>
									</tr>
									<tr>
										<th>Energieausweis</th>
										<th>:</th>
										<td><input type="text" placeholder="Energieausweis" v-model="newWohnung.Energieausweis"/></td>
									</tr>
									<tr>
										<th>Energieausweistyp</th>
										<th>:</th>
										<td><input type="text" placeholder="Energieausweistyp" v-model="newWohnung.Energieausweistyp"/></td>
									</tr>
									<tr>
										<th>Energiekennwert</th>
										<th>:</th>
										<td><input type="text" placeholder="Energieverbrauchskennwert" v-model="newWohnung.Energiekennwert"/></td>
									</tr>
								</table>
							</v-tab>
							<v-tab title="Objekt-Kosten">
								<table class="form">
									<tr>
										<th>Kaltmiete</th>
										<th>:</th>
										<td><input type="number" placeholder="Kaltmiete" v-model="newWohnung.Kaltmiete"/></td>
									</tr>
									<tr>
										<th>Nebenkosten</th>
										<th>:</th>
										<td><input type="number" placeholder="Nebenkosten" v-model="newWohnung.Nebenkosten"/></td>
									</tr>
									<tr>
										<th>Heizkosten</th>
										<th>:</th>
										<td><input type="number" placeholder="Heizkosten" v-model="newWohnung.Heizkosten"/></td>
									</tr>
									<tr>
										<th>Gesamtkosten</th>
										<th>:</th>
										<td><input type="number" placeholder="Gesamtkosten" v-model="newWohnung.Gesamtkosten"/></td>
									</tr>
								</table>
							</v-tab>							
						</vue-tabs>
						<div class="margin"></div>
						<button class="center"  @click="showingModal = false; saveWohnung();" >Wohnung hinzufügen</button>
					</div>
				</div>
			</div>
		<div class="modal col-md-10" id="editmodal" v-if="showingeditModal">
				<div class="modalheading">
					<p class="left">Wohnung Bearbeiten</p>
					<p class="right close" @click="showingeditModal = false;">x</p>
					<div class="fix"></div>
				</div>
				<div class="modalbody">
					<div class="modalcontent">
						<vue-tabs active-tab-color="#0074E8"  active-text-color="white">
							<v-tab title="Objekt-Daten">
								<table class="form">
									<tr>
										<th>WohnungID</th>
										<th>:</th>
										<td><input type="text" placeholder="Wohnung" v-model="actWohnung.Objekt"/></td>
									</tr>
									<tr>
										<th>Beschreibung</th>
										<th>:</th>
										<td><textarea type="text" placeholder="Beschreibung" v-model="actWohnung.ObjektBeschreibung"/></textarea></td>
									</tr>
									<tr>
										<th>Strasse</th>
										<th>:</th>
										<td><input type="text" placeholder="Straße" v-model="actWohnung.w_Strasse"/></td>
									</tr>
									<tr>
										<th>PLZ</th>
										<th>:</th>
										<td><input type="text" placeholder="PLZ" v-model="actWohnung.w_PLZ"/></td>
									</tr>
									<tr>
										<th>Ort</th>
										<th>:</th>
										<td><input type="text" placeholder="Ort" v-model="actWohnung.w_Ort"/></td>
									</tr>
									<tr>
										<th>Land</th>
										<th>:</th>
										<td><input type="text" placeholder="Land" v-model="actWohnung.w_Land"/></td>
									</tr>
									<tr>
										<th>Kontakt E-Mail Adresse</th>
										<th>:</th>
										<td><input type="email" placeholder="Email" v-model="actWohnung.w_email"/></td>
									</tr>
								</table>
								<div class="margin"></div>
								<button class="center"  @click="showingeditModal = false; updateWohnung()">Änderungen speichern</button>
							</v-tab>
							<v-tab title="Objekt-Details">
								<table class="form">
									<tr>
										<th>Bezugsfrei ab</th>
										<th>:</th>
										<td><input type="date"  v-model="actWohnung.Bezugsfreiab"/></td>
									</tr>
									<tr>
										<th>Objektzustand</th>
										<th>:</th>
										<td><input type="text" placeholder="Objektzustand" v-model="actWohnung.Objektzustand"/></td>
									</tr>
									<tr>
										<th>Baujahr</th>
										<th>:</th>
										<td><input type="number" placeholder="Baujahr" v-model="actWohnung.Baujahr"/></td>
									</tr>
									<tr>
										<th>Modernisiert am<br>Saniert am</th>
										<th>:</th>
										<td><input type="date" v-model="actWohnung.Modern_Sanierung"/></td>
									</tr>
									<tr>
										<th>Typ</th>
										<th>:</th>
										<td><input type="text"  v-model="actWohnung.Typ"></td>
									</tr>
									<tr>
										<th>Etage</th>
										<th>:</th>
										<td><input type="text" placeholder="Etage" v-model="actWohnung.Etage"/></td>
									</tr>
									<tr>
										<th>Wohnfläche</th>
										<th>:</th>
										<td><input type="number" placeholder="Wohnfläche" v-model="actWohnung.Wohnflaeche"/></td>
									</tr>
									<tr>
										<th>Nutzfläche</th>
										<th>:</th>
										<td><input type="number" placeholder="Nutzfläche" v-model="actWohnung.Nutzflaeche"/></td>
									</tr>
									<tr>
										<th>Zimmer</th>
										<th>:</th>
										<td>
											<select v-model="actWohnung.ZimmerID">
												<option v-for="Z in Zimmer" v-bind:value="Z.ZID" selected="actWohnung.ZimmerID == Z.ZID ? 'selected' : ''">{{Z.Anzahl}}</option>
											</select>
										</td>
									</tr>
									<tr>
										<th>Schlafzimmer</th>
										<th>:</th>
										<td><input type="number" placeholder="Schlafzimmer" v-model="actWohnung.Schlafzimmer"/></td>
									</tr>
									<tr>
										<th>Badezimmer</th>
										<th>:</th>
										<td><input type="number" placeholder="Badezimmer" v-model="actWohnung.Badezimmer"/></td>
									</tr>
									<tr>
										<th>Haustiere</th>
										<th>:</th>
										<td><input type="text" placeholder="Haustiere" v-model="actWohnung.Haustiere"/></td>
									</tr>
								</table>
								<div class="margin"></div>
								<button class="center"  @click="showingeditModal = false; updateWohnung()">Änderungen speichern</button>
							</v-tab>
							<v-tab title="Energiedetails">
								<table class="form">
									<tr>
										<th>Heizungssart</th>
										<th>:</th>
										<td><input type="text" placeholder="Heizungssart" v-model="actWohnung.Heizungssart"/></td>
									</tr>
									<tr>
										<th>Energieträger</th>
										<th>:</th>
										<td><input type="text" placeholder="z.B. Gas" v-model="actWohnung.Energietraeger"/></td>
									</tr>
									<tr>
										<th>Energieausweis</th>
										<th>:</th>
										<td><input type="text" placeholder="Energieausweis" v-model="actWohnung.Energieausweis"/></td>
									</tr>
									<tr>
										<th>Energieausweistyp</th>
										<th>:</th>
										<td><input type="text" placeholder="Energieausweistyp" v-model="actWohnung.Energieausweistyp"/></td>
									</tr>
									<tr>
										<th>Energiekennwert</th>
										<th>:</th>
										<td><input type="text" placeholder="Energieverbrauchskennwert" v-model="actWohnung.Energiekennwert"/></td>
									</tr>
								</table>
								<div class="margin"></div>
								<button class="center"  @click="showingeditModal = false; updateWohnung()">Änderungen speichern</button>
							</v-tab>
							<v-tab title="Objekt-Kosten">
								<table class="form">
									<tr>
										<th>Kaltmiete</th>
										<th>:</th>
										<td><input type="number" placeholder="Kaltmiete" v-model="actWohnung.Kaltmiete"/></td>
									</tr>
									<tr>
										<th>Nebenkosten</th>
										<th>:</th>
										<td><input type="number" placeholder="Nebenkosten" v-model="actWohnung.Nebenkosten"/></td>
									</tr>
									<tr>
										<th>Heizkosten</th>
										<th>:</th>
										<td><input type="number" placeholder="Heizkosten" v-model="actWohnung.Heizkosten"/></td>
									</tr>
									<tr>
										<th>Gesamtkosten</th>
										<th>:</th>
										<td><input type="number" placeholder="Gesamtkosten" v-model="actWohnung.Gesamtkosten"/></td>
									</tr>
								</table>
								<div class="margin"></div>
								<button class="center"  @click="showingeditModal = false; updateWohnung()">Änderungen speichern</button>
							</v-tab>							
							<v-tab title="Picture hochladen">
							<div class="container" style="float:left;width:200px">
							</div>
							<label>Bilder
								<input type="file" id="files" ref="files" multiple @change="handleFilesUpload()"/>
							</label>
							<button v-on:click="addFiles">Hinzufügen</button>
							<button v-on:click="submitFiles">Hochladen</button>
							<div  style="width:300px;height:600px;float:left;overflow-y:scroll;overflow-x:hidden;padding-right:20px">
								<ul  class="file-listing">
									<li v-for="(file, key) in files">
									<img class="preview" v-bind:ref="'image' +parseInt( key )" style="width:75px;height:75px" /> 
									{{ file.name.substr(0,40) }} <span class="remove-file" v-on:click="removeFile( key )"><b>Löschen</b></span>
									</li>
								</ul>
							</div>
							<div id="image" style="float:right;width:655px;height:600px;overflow-y:scroll;overflow-x:hidden">
								<table style="width:100%" >
									<tr v-for="b in Picture" v-if="b.WID == actWohnung.WID">
										<td valign="bottom"><image :title="b.Name" :src="b.File" style="width:250px;padding:5px;"></td>
										<td valign="top" style="padding:10px">
											<input type="radio" name="front" :id="b.BID"  @click="saveFront(b.File)" :checked="`${b.Frontpicture == 1 ? 'checked' : ''}`"/>
											<label :for="b.BID">Im Front anzeigen</label>
										</td>
										<td valign="top" style="padding:10px">
											<button class="left" @click="deletePicture(b.File)" >Löschen</button><br><br>
										</td>
									</tr>
								</table>
							</div>
							</v-tab>						
						</vue-tabs>
					</div>
				</div>
			</div>
			<div class="modal col-md-6" id="deletemodal" v-if="showingdeleteModal">
				<div class="modalheading">
					<p class="left">Wohnung Löschen</p>
					<p class="right close" @click="showingdeleteModal = false;">x</p>
					<div class="fix"></div>
				</div>
				<div class="modalbody">
					<div class="modalcontent">
						<div class="margin"></div>
						<h3 class="center">Sind Sie sicher, möchten Sie wirklich löschen?</h3>
						<div class="margin"></div>
						<h4 class="center">{{actWohnung.Objekt}}</h4>
						<div class="margin"></div>
						<div class="col-md-6 center">
							<button class="left" @click="showingdeleteModal = false; deleteWohnung()">Ja</button>
							<button class="right" @click="showingdeleteModal = false;">Nein</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="js/vue.min.js"></script>
	<script type="text/javascript" src="js/vuepack.js"></script>
	<script type="text/javascript" src="app_admin.js"></script>
</body>
</html>