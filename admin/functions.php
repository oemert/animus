<?php
function uml_replace( $text ){
	$text = str_replace("ä", "&auml;", utf8_decode($text));
	$text = str_replace("ö", "&ouml;", $text);
	$text = str_replace("ü", "&uuml;", $text);
	$text = str_replace("Ä", "&Auml;", $text);
	$text = str_replace("Ö", "&Ouml;", $text);
	$text = str_replace("Ü", "&Uuml;", $text);
	$text = str_replace("ß", "&szlig;", $text);
	return $text;
}

function umlaute( $text ){
	$text = str_replace("&auml;","ä", utf8_decode($text));
	$text = str_replace( "&ouml;","ö", $text);
	$text = str_replace( "&uuml;","ü", $text);
	$text = str_replace( "&Auml;","Ä", $text);
	$text = str_replace( "&Ouml;","Ö", $text);
	$text = str_replace( "&Uuml;","Ü", $text);
	$text = str_replace( "&szlig;","ß", $text);
	return $text;
}

function zufallsstring($laenge) {
	   //Mögliche Zeichen für den String
	   $zeichen = '0123456789';
	   $zeichen .= 'abcdefghijklmnopqrstuvwxyz';
	   $zeichen .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	   $zeichen .= '_!()';
	 
	   //String wird generiert
	   $str = '';
	   $anz = strlen($zeichen);
	   for ($i=0; $i<$laenge; $i++) {
		  $str .= $zeichen[rand(0,$anz-1)];
	   }
   return $str;
}	
?>